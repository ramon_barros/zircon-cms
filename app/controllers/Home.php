<?php
 
namespace app\controllers;
//CRUD
Class Home extends \Zircon\Core\Controller
{
    public function create()
    {
        echo 'Home Create';
    }

    public function read()
    {
        echo "Home Read";
    }

    public function update() {
        echo "Home Update";
    }

    public function delete() {
        echo "Home Delete";
    }
}