<?php
 
namespace app\controllers;
 
Class Main extends \Zircon\Core\Controller
{
    public function actionIndex()
    {
        echo $this->data->message;
    }
 
    public function actionTest()
    {
        echo "Controller Main actionTest";
    }
}