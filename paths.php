<?php
/**
 * Step 1: Require the Slim Framework
 *
 * If you are not using Composer, you need to require the
 * Slim Framework and register its PSR-0 autoloader.
 *
 * If you are using Composer, you can skip this step.
 */
require 'zircon/vendor/Slim/Slim.php';

\Slim\Slim::registerAutoloader();

/**
 * Step 2: Instantiate a Slim application
 *
 * This example instantiates a Slim application using
 * its default settings. However, you will usually configure
 * your Slim application now by passing an associative array
 * of setting names and values into the application constructor.
 */
//$app = new \Slim\Slim();

ini_set('display_errors', 1);
error_reporting(E_ALL | E_STRICT );
date_default_timezone_set('America/Sao_Paulo');

define('DS', DIRECTORY_SEPARATOR);
define('APP_ROOT', realpath(__DIR__.DS));
define('EXT', '.php');
$time = time();
header('Last-Modified: '.gmdate('D, d M Y H:i:s', $time).'GMT');

require 'zircon/core/App.php';

\Zircon\Core\App::registerAutoloader();

$router = new \Zircon\Core\Router;
 
$routes = array(
    'Home:create@post' => '/',
    'Home:read@get' => '/',
    'Home:update@put' => '/',
    'Home:delete@delete' => '/',
    'Main:actionIndex@get' => '/main',
    'Main:actionTest@get' => '/main/test',
    'Foo:index@get' => '/foo',
);
 
$router->addRoutes($routes);
 
$router->detect();

$router->run();