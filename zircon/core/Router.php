<?php

namespace Zircon\Core;

use FilesystemIterator as fIterator;

Class Router {

    protected $controllers = array();
    protected $routes;
    protected $request;
 
    public function __construct()
    {
        $env = \Slim\Environment::getInstance();
        $this->request = new \Slim\Http\Request($env);
        $this->routes = array();
    }

    public function detect($directory = null) {
        if (is_null($directory)) {
            $directory = APP_ROOT . DS . 'app' . DS .'controllers';
        }

        $root = $directory = APP_ROOT . DS . 'app' . DS .'controllers' . DS;

        $controllers = array();

        $items = new fIterator($directory, fIterator::SKIP_DOTS);

        foreach ($items as $item)
        {
            if ($item->isDir())
            {
                $nested = static::detect($item->getRealPath());

                $controllers = array_merge($controllers, $nested);
            } else {
                $controller = str_replace(array($root, EXT), '', $item->getRealPath());

                $controller = str_replace(DS, '.', $controller);

                $controllers[] = $controller;
            }
        }

        var_dump($controllers);
    }

    public function addRoutes($routes)
    {
        foreach ($routes as $path => $route) {

            $method = "any";

            if (strpos($path, "@") !== false) {
                list($path, $method) = explode("@", $path);
            }

            $func = $this->processCallback($path);

            $r = new \Slim\Route($route, $func);
            $r->setHttpMethods(strtoupper($method));

            array_push($this->routes, $r);
        }
    }

    protected function processCallback($path)
    {
        $class = "Main";
     
        if (strpos($path, ":") !== false) {
            list($class, $path) = explode(":", $path);
        }
        
        $function = ($path != "") ? $path : "index";

        $func = function () use ($class, $function) {
            if (file_exists(APP_ROOT . DS. 'app' . DS . 'controllers' . DS . $class.'.php')) {
                $class = 'app\controllers\\' . $class;
                $class = new $class();
     
                $args = func_get_args();
     
                return call_user_func_array(array($class, $function), $args);
            } else {
                return false;
            }
        };
        //echo "<pre>";
        //var_dump($func);
        return $func;
    }

    public function run()
    {
        $display404 = true;
        $uri = $this->request->getResourceUri();
        $method = $this->request->getMethod();
        var_dump($uri);
        var_dump($method);

        foreach ($this->routes as $i => $route) {
            if ($route->matches($uri)) {
                if ($route->supportsHttpMethod($method) || $route->supportsHttpMethod("ANY")) {
                    $call = call_user_func_array($route->getCallable(), array_values($route->getParams()));
                    $call === false ? $display404 = true : $display404 = false;
                }
            }
        }
     
        if ($display404) {
            echo "404 - route not found";
        }
    }
}