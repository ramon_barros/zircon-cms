<?php
 
namespace Zircon\Core;
 
abstract Class Controller extends \Slim\Slim
{
    public $restful = true;

    protected $data;
 
    public function __construct()
    {
        //$settings = require("../settings.php");
        $settings = array(
            //'view' => new \Slim\Extras\Views\Twig(),
            'templates.path' => '../Views',
            'model' => (Object)array(
                "message" => "Hello World"
            )
        );
        if (isset($settings['model'])) {
            $this->data = $settings['model'];
        }
        parent::__construct($settings);
    }
}